import http from 'k6/http';
import { sleep } from 'k6';
import { Rate } from 'k6/metrics';


const myFailRate = new Rate('failed requests');


export let options = {
  duration: "1m",
  vus: 50,
  // Thresholds docs https://k6.io/docs/using-k6/thresholds
  thresholds: {
    'failed requests': ['rate>=0.9'], // threshold on a custom metric ... more or equal to 90%

    'http_req_duration': ['p(95)<500']  // threshold on a standard metric    

    // Other examples
    // 90% of requests must finish within 400ms, 95% within 800, and 99.9% within 2s.
    // 'http_req_duration': ['p(90) < 400', 'p(95) < 800', 'p(99.9) < 2000'],  
  }
};


export default function() {
  let res = http.get(`${__ENV.TEST_HOSTNAME}`);
  myFailRate.add(res.status !== 200);

  sleep(1);
}
